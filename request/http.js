const base_url = 'https://api.zbztb.cn/api/public/v1'
let ajaxTimes = 0
export const request = (parmas) => {
    ajaxTimes++
    wx.showLoading({
        title: '加载中',
        mask: true
    })
    return new Promise((resolve, reject) => {
        wx.request({
            ...parmas,
            url: base_url + parmas.url,
            success: (response) => {
                resolve(response)
            },
            fail: (err) => {
                reject(err)
            },
            complete: () => {
                ajaxTimes--
                if(ajaxTimes === 0) {
                    wx.hideLoading()
                }
            }
        })
    })
}