import { request } from '../../request/http'
Page({
  data: {
    tabs: [{
      id: 0,
      value: '全部订单',
      isActive: true
    }, {
      id: 1,
      value: '待付款',
      isActive: false
    }, {
      id: 2,
      value: '待发货',
      isActive: false
    }, {
      id: 3,
      value: '退款/退货',
      isActive: false
    }],
    orders: []
  },

  onLoad: function (options) {

  },

  onShow() {
    const token = wx.getStorageSync("token")
    if (!token) {
      wx.navigateTo({
        url: '/pages/auth/auth'
      })
      return
    }
    //获取小程序页面栈，最大长度为10
    let page = getCurrentPages()
    let currentPage = page[page.length - 1]
    this.changeTitleByIndex(currentPage.options.type - 1)
    this.getOrders(currentPage.options.type)
  },

  getOrders(type) {
    request({ url: '/my/orders/all', data: { type } }).then(res => {
      let orders = res.data.message.orders.map(item => {
        return {
          ...item,
          create_time_cn: (new Date(item.create_time * 1000).toLocaleDateString())
        }
      })
      this.setData({
        orders: res.data.message.orders
      })
    })
  },

  changeTitleByIndex(index) {
    let { tabs } = this.data
    tabs.forEach((item, i) => {
      if (i === index) {
        item.isActive = true
      } else {
        item.isActive = false
      }
    });
    this.setData({
      tabs
    })
  },

  handleTabsItemChange(e) {
    const { index } = e.detail
    this.changeTitleByIndex(index)
    this.getOrders(index + 1)
  }
})