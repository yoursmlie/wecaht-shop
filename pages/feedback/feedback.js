Page({
  data: {
    tabs: [{
      id: 0,
      value: '体验问题',
      isActive: true
    }, {
      id: 1,
      value: '商家投诉',
      isActive: false
    }],
    chooseImgs: [],
    textValue: ''
  },

  UpLoadImgs: [],

  handleChooseImg(e) {
    wx.chooseImage({
      count: 9,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: (res) => {
        this.setData({
          chooseImgs: [...this.data.chooseImgs, ...res.tempFilePaths]
        })
      }
    })
  },

  handleRemoveImg(e) {
    const { index } = e.currentTarget.dataset
    let { chooseImgs } = this.data
    chooseImgs.splice(index, 1)
    this.setData({
      chooseImgs
    })
  },

  handleTextInput(e) {
    this.setData({
      textValue: e.detail.value
    })
  },

  handleFormSubmit() {
    const { textValue, chooseImgs } = this.data
    if (!textValue.trim()) {
      wx.showToast({
        title: '输入不合法',
        icon: 'none',
        mask: true
      })
      return
    }

    wx.showLoading({
      title: '正在上传中',
      mask: true
    })

    if (chooseImgs.length !== 0) {
      //上传文件不支持多个文件同时上传
      chooseImgs.forEach((item, index) => {
        wx.uploadFile({
          url: 'https://images.ac.cn/Home/Index/UploadAction/', //上传到哪里
          filePath: item, //上传文件的路径
          name: 'file', //上传文件的名称
          formData: {}, //文本信息
          success: (res) => {
            let url = JSON.parse(res.data).url
            this.UpLoadImgs.push(url)
            if (index === chooseImgs.length - 1) {
              console.log('把文本内容和外网图片发送给后台')
              this.setData({
                textValue: '',
                chooseImgs: []
              })
              wx.hideLoading()
              wx.navigateBack({
                delta: 1
              })
            }
          }
        })
      })
    }else {
      wx.hideLoading()
      console.log('提交文本')
      wx.navigateBack({
        delta: 1
      })
    }
  }

})