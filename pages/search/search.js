import { request } from '../../request/http'
Page({
  data: {
    goods: [],
    isFocus: false,
    inputValue: ''
  },

  timer: null,

  handleInput(e) {
    clearTimeout(this.timer)
    const { value } = e.detail
    if (!value.trim()) {
      this.setData({
        goods: [],
        isFocus: false
      })
      return
    }
    this.setData({
      isFocus: true,
      inputValue: value
    })
    this.timer = setTimeout(() => {
      request({ url: "/goods/qsearch", data: { query: value } }).then(res => {
        this.setData({
          goods: res.data.message
        })
      })
    }, 1000);
  },
  handleCanel() {
    this.setData({
      goods: [],
      isFocus: false,
      inputValue: ''
    })
  }
})