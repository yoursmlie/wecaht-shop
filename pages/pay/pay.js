import regeneratorRuntime from '../../lib/runtime/runtime'
import { request } from '../../request/http'
import { WxPay } from '../../utils/utils'

Page({
  data: {
    address: {},
    cart: [],
    totalPrice: 0,
    totalNUm: 0
  },

  onLoad: function (options) {

  },

  onShow() {
    const address = wx.getStorageSync('address')
    let cart = wx.getStorageSync("cart") || []
    cart = cart.filter(item => item.checked)
    this.setData({
      address
    })
    this.setCart(cart)
  },

  setCart(cart) {
    let totalPrice = 0;
    let totalNUm = 0;
    cart.forEach(item => {
      totalPrice += item.num * item.goods_price
      totalNUm += item.num
    })
    this.setData({
      cart,
      totalPrice,
      totalNUm
    })
  },

  async handleOrderPay() {
    //判断是否存在token
    const token = wx.getStorageSync("token")
    if (!token) {
      wx.navigateTo({
        url: '/pages/auth/auth'
      })
      return
    }
    //如果存在Token
    //创建订单
    const header = { Authorization: token }
    const order_price = this.data.totalPrice
    const consignee_addr = this.data.address.all
    const cart = this.data.cart
    let goods = []
    cart.forEach(item => {
      goods.push({
        goods_id: item.goods_id,
        goods_number: item.goods_number,
        goods_price: item.goods_price
      })
    })
    let orderParams = {
      order_price,
      consignee_addr,
      goods
    }
    let { order_number } = await request({
      url: '/my/orders/create',
      method: 'POST',
      data: { orderParams },
      header
    })
    //准备预支付
    if(order_number) {
      let { pay } = await request({
        url: '/my/orders/req_unifiedorder', 
        method: 'POST', 
        header,
        data: {order_number}
      })
      //发起微信支付
      if(pay) {
        let response = await WxPay()
        //查询订单状态是否成功
        let res = request({
          url: '/my/orders/chkOrder',
          method: 'POST',
          header,
          data: { orderParams },
        })
        wx.showToast({
          title: '支付成功',
          icon: 'success',
          icon: 'none',
          duration: 2000
        })
        let newCart = wx.getStorageSync('cart')
        newCart = newCart.filter(item => !item.checked)
        wx.setStorageSync('cart', newCart)
        wx.navigateTo({
          url: '/pages/order/order'
        })
      }
    }
  }
})