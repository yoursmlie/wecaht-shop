import { request } from '../../request/http'
import { WxLogin } from '../../utils/utils'
import regeneratorRuntime from '../../lib/runtime/runtime'
Page({
  data: {

  },

  onLoad: function (options) {

  },

  async getUserInfo(e) {
    //获取Token必要的用户信息
    const { encryptedData, rawData, iv, signature } = e.detail
    //获取小程序登录成功后的code值
    const { code } = await WxLogin()
    const LoginParams =  {
      encryptedData,
      rawData,
      iv,
      signature,
      code
    }
    const { token } = await request({url: '/users/wxlogin', data: LoginParams, method: "post"})
    if(token) {
      WxLogin.setStorage("token", token)
      WxLogin.navigateBack({
        delta: 1
      })
    }
  }
})