import { request } from '../../request/http'
Page({
  data: {
    goodsDetail: {},
    isCollect: false
  },

  GoodsInfo: {},

  onShow: function () {
    let Pages = getCurrentPages()
    let currentPages = Pages[Pages.length - 1]
    let options = currentPages.options
    const { goods_id } = options
    this.getGoodsDetailData(goods_id)
  },

  getGoodsDetailData(goods_id) {
    request({ url: '/goods/detail', data: { goods_id } }).then(res => {
      this.GoodsInfo = res.data.message
      let collect = wx.getStorageSync("collect") || []
      let isCollect = collect.some(item => item.goods_id === this.GoodsInfo.goods_id)
      this.setData({
        goodsDetail: {
          goods_name: res.data.message.goods_name,
          goods_price: res.data.message.goods_price,
          goods_introduce: res.data.message.goods_introduce,
          pics: res.data.message.pics
        },
        isCollect
      })
    })
  },

  handlePreviewImage(e) {
    const urls = this.GoodsInfo.pics.map(item => {
      return item.pics_mid
    })
    const current = e.currentTarget.dataset.url;
    wx.previewImage({
      current,
      urls
    });
  },

  handleAddCart() {
    let cart = wx.getStorageSync("cart") || []
    let index = cart.findIndex(item => item.goods_id === this.GoodsInfo.goods_id)
    if (index === -1) {
      this.GoodsInfo.num = 1
      this.GoodsInfo.checked = true
      cart.push(this.GoodsInfo)
    } else {
      cart[index].num++
    }

    wx.setStorageSync("cart", cart)

    wx.showToast({
      title: '加入购物车成功',
      duration: 2000
    })
  },

  handleCollect() {
    let isCollect = false
    let collect = wx.getStorageSync("collect") || []
    let index = collect.findIndex(item => item.goods_id === this.GoodsInfo.goods_id)
    if(index !== -1) {
      collect.splice(index, 1)
      isCollect = false
      wx.showToast({
        title: '取消收藏成功',
        icon: 'success',
        mask: true
      })
    }else {
      collect.push(this.GoodsInfo)
      isCollect = true
      wx.showToast({
        title: '收藏成功',
        icon: 'success',
        mask: true
      })
    }
    this.setData({
      isCollect
    })
    wx.setStorageSync("collect", collect)
  }
})