import { request } from '../../request/http'
Page({
  data: {
    tabs: [{
      id: 0,
      value: '综合',
      isActive: true
    },{
      id: 1,
      value: '销量',
      isActive: false
    },{
      id: 2,
      value: '价格',
      isActive: false
    }],
    goodsList: []
  },

  Params: {
    query: '',
    cid: '',
    pagenum: 1,
    pagesize: 10,
    totalPage: 1
  },

  onLoad: function (options) {
    this.Params.cid = options.cid
    this.getGoodsListData()
  },

  getGoodsListData() {
    request({url: "/goods/search", data: this.Params}).then(res => {
      const total = res.data.message.total
      this.Params.totalPage = Math.ceil(total / this.Params.pagesize)
      this.setData({
        goodsList: [...this.data.goodsList, ...res.data.message.goods]
      })
      wx.stopPullDownRefresh()
    })
  },

  tabsItemChange(e) {
    const { index } = e.detail
    let { tabs } = this.data
    tabs.forEach((item, i) => {
        if(i === index) {
          item.isActive = true
        }else {
          item.isActive = false
        }
    })
    this.setData({
      tabs
    })
  },

  onReachBottom() {
    if(this.Params.pagenum >= this.Params.totalPage) {
      wx.showToast({
        title: '已经是最后一页',
        duration: 2000
      })
    }else {
      this.Params.pagenum++
      this.getGoodsListData()
    }
  },

  onPullDownRefresh() {
    this.setData({
      goodsList: []
    })
    this.Params.pagenum = 1
    this.getGoodsListData()
  }
})