Page({
  data: {

  },
  onLoad: function (options) {

  },
  getUserInfo(e) {
    const { userInfo } = e.detail
    wx.setStorageSync("userinfo", userInfo)
    wx.navigateBack({
      delta: 1
    })
  }
})