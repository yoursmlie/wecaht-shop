import { request } from '../../request/http'
Page({

  data: {
    swiperList: [],
    catesList: [],
    floorList: []
  },

  //页面开始加载触发
  onLoad: function (options) {
    this.getHomeData()
  },

  getHomeData() {
    Promise.all([
      request({url: '/home/swiperdata'}), 
      request({url: '/home/catitems'}),
      request({url: '/home/floordata'})
    ]).then(res => {
      this.setData({
        swiperList: res[0].data.message,
        catesList: res[1].data.message,
        floorList: res[2].data.message
      })
    })
  }
})