import { getSetting, chooseAddress, openSetting, showModal } from '../../utils/utils'
import regeneratorRuntime from '../../lib/runtime/runtime'

Page({
  data: {
    address: {},
    cart: [],
    allChecked: false,
    totalPrice: 0,
    totalNUm: 0
  },

  onLoad: function (options) {

  },

  async handleChooseAddress() {
    try {
      const setting = await getSetting()
      const scopeAddress = setting.authSetting["scope.address"]
      if (scopeAddress === false) {
        await openSetting()
      }
      const address = await chooseAddress()
      address.all = address.provinceName + address.cityName + address.countyName + address.detailInfo
      wx.setStorageSync("address", address)
    } catch (error) {
      console.log(error)
    }
  },

  onShow() {
    const address = wx.getStorageSync('address')
    const cart = wx.getStorageSync("cart") || []
    this.setData({
      address
    })
    this.setCart(cart)
  },

  handeItemChange(e) {
    const goods_id = e.currentTarget.dataset.id
    let { cart } = this.data
    let index = cart.findIndex(item => item.goods_id === goods_id)
    cart[index].checked = !cart[index].checked
    this.setData({
      cart
    })
    this.setCart(cart)
  },

  handleItemAllChecked() {
    let { cart, allChecked } = this.data
    allChecked = !allChecked
    cart.forEach(item => {
      return item.checked = allChecked
    })
    this.setCart(cart)
  },

  handleNumEdit(e) {
    const { operation, id } = e.currentTarget.dataset
    let { cart } = this.data
    const index = cart.findIndex(item => item.goods_id === id)
    if (cart[index].num === 1 && operation === -1) {
      showModal({content: '是否删除该商品'}).then(res => {
        if(res.confirm) {
          cart.splice(index, 1)
          this.setCart(cart)
        }else {
          return
        }
      })
    }else {
      cart[index].num += operation
      this.setCart(cart)
    }
  },

  handlePay() {
    const { address, totalNUm } = this.data
    if(!address.userName) {
      wx.showToast({
        title: '请添加收获地址',
        icon: 'none'
      })
      return
    }
    if(totalNUm === 0) {
      wx.showToast({
        title: '还未添加商品',
        icon: 'none'
      })
      return
    }
    wx.navigateTo({
      url: '/pages/pay/pay'
    })
  },

  setCart(cart) {
    let totalPrice = 0;
    let totalNUm = 0;
    let allChecked = cart.length ? true : false;
    cart.forEach(item => {
      if (item.checked) {
        totalPrice += item.num * item.goods_price
        totalNUm += item.num
      } else {
        allChecked = false
      }
    })
    this.setData({
      cart,
      allChecked,
      totalPrice,
      totalNUm
    })
    wx.setStorageSync("cart", cart)
  }
})