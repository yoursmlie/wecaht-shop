import { request } from '../../request/http'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    leftMenuList: [],
    rightContent: [],
    Cates: [],
    currentIndex: 0,
    scrollTop: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {  
    const cates = wx.getStorageSync("cates")
    if(!cates) {
      this.getCateGoryData()
    }else {
      if(Date.now() - cates.time > 1000 * 10) {
        this.getCateGoryData()
      }else {
        this.setData({
          Cates: cates.data
        })
        this.UpdateData()
      }
    }
  },

  getCateGoryData() {
    request({url: '/categories'}).then(res => {
      this.setData({
        Cates: res.data.message
      })
      this.UpdateData()
      wx.setStorageSync("cates", {time: Date.now(), data: res.data.message})
    })
  },

  UpdateData() {
    let leftMenuList = this.data.Cates.map(item => item.cat_name)
    let rightContent = this.data.Cates[0].children
    this.setData({
      leftMenuList,
      rightContent
    })
  },

  handleItemTap(e) {
    const { index } = e.currentTarget.dataset
    this.setData({
      currentIndex: index,
      rightContent: this.data.Cates[index].children,
      scrollTop: 0
    })
  }
  
})